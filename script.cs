using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class script : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform point;
    public NavMeshAgent agent;
    void Start()
    {
        agent= GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {



        agent.SetDestination(point.position);
        //creation de raycast et le test du raycast
        Debug.DrawRay(transform.position, transform.forward * 5, Color.red);
        Debug.DrawRay(transform.position, transform.right * 5, Color.red);
        Debug.DrawRay(transform.position, -transform.right * 5, Color.red);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 10))
        {
            Debug.Log(hit.transform.gameObject.name);

        }


    }
}